const readline = require("readline");

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

const question = function (question, excecute) {
	rl.question(question, excecute);
};

rl.on("close", () => {
	process.exit();
});

const close = function () {
	rl.close();
};

module.exports.question = question;
module.exports.close = close;
