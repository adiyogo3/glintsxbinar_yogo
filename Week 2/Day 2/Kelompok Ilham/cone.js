const { runInContext } = require("vm");
const index = require("./question.js");
const rl = index.rl;

function isEmptyOrSpaces(str) {
	return str === null || str.match(/^ *$/) !== null || str === "0";
}

function cone() {
	console.clear();
	console.log(`CONE'S VOLUME`);
	console.log(`=========`);
	inputRadius();
}

function calculateCone(radius, height) {
	const phi = 3.14;
	const r = radius ** 2;
	return (1 / 3) * phi * r * height;
}

function inputRadius() {
	index.question(`enter a radius (r) value : `, (radius) => {
		if (!isNaN(radius) && !isEmptyOrSpaces(radius)) {
			inputHeight(radius);
		} else if (isEmptyOrSpaces(radius)) {
			console.log(`value cannot be empty or 0\n`);
			inputRadius();
		} else {
			console.log(`Radius must be a number\n`);
			inputRadius();
		}
	});
}

function inputHeight(radius) {
	index.question(`enter a height (t) value : `, (height) => {
		if (!isNaN(height) && !isEmptyOrSpaces(height)) {
			console.log(
				`The volume of a cone is ${calculateCone(radius, height)}\n`
			);
			index.close();
			// quest();
		} else if (isEmptyOrSpaces(height)) {
			console.log(`value cannot be empty or 0\n`);
			inputHeight(radius);
		} else {
			console.log(`Height must be a number\n`);
			inputHeight(radius);
		}
	});
}

function quest() {
	console.log(`=========`);
	index.question("do you want to calculate again? y/n : ", (answers) => {
		if (answers == "y") {
			index.menu();
		} else if (answers == "n") {
			index.rl.close();
		} else {
			console.log("Sorry, there's no option for that!");
			quest();
		}
	});
}

module.exports.cone = cone;
