const index = require("./question.js");

function isEmptyOrSpaces(str) {
	return str === null || str.match(/^ *$/) !== null || str === "0";
}

function ball() {
	console.clear();
	console.log(`Ball Volume`);
	console.log(`=========`);
	inputRadius();
}
function calculateVolumeBall(radius) {
	// V = 4/3 * Pi * r**3
	const phi = 3.14;
	const r = radius ** 3;
	return (4 / 3) * phi * r;
}

function inputRadius() {
	index.question(`enter a radius (r) value : `, (radius) => {
		if (!isNaN(radius) && !isEmptyOrSpaces(radius)) {
			console.log(`ball is ${calculateVolumeBall(radius)}`);
			index.close();
		} else {
			console.log(`Radius must be a number\n`);
			inputRadius();
		}
	});
}

module.exports.ball = ball;
