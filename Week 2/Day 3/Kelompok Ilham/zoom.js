const readline = require("readline");
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

let hasLink = false;

const mulaiAplikasi = function () {
	console.clear();
	console.log("Aplikasi Zoom Dimulai .....");
	checkZoomLink();
};

const checkZoomLink = function () {
	if (hasLink) {
		joinMeeting();
	} else {
		absenDulu();
	}
};

const absenDulu = function () {
	rl.question("Masukan Nama : ", (nama) => {
		rl.question("Masukan Email : ", (email) => {
			hasLink = true;
			joinMeeting();
		});
	});
};

const joinMeeting = function () {
	console.clear();
	let link = "entahlah";
	let pass = "12345";
	rl.question("Masukan Meeting ID : ", (meetingID) => {
		rl.question("Masukan Password Meeeting : ", (meetingPass) => {
			if (meetingID === link && meetingPass === pass) {
				console.log("Memasuki Ruangan Meeting");
				rl.close();
				console.log("Accepted");
			} else {
				//
				rl.question(
					"Meeting ID atau Password yang dimasukan salah...",
					() => {
						joinMeeting();
					}
				);
			}
		});
	});
};

rl.on("close", () => {
	process.exit();
});

mulaiAplikasi();
