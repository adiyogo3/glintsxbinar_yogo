const express = require('express') // import express
const router = express.Router()
const PelangganController = require('../controllers/pelangganController.js')

router.get('/', PelangganController.getAll) // if acessing localhost:3000/pelanggan, it will do function getAll() in pelangganController class
router.get('/:id', PelangganController.getOne) // if acessing localhost:3000/pelanggan/:id, it will do function getOne() in pelangganController class
router.post('/create', PelangganController.create) // if acessing localhost:3000/create, it will do function create() in pelangganController class
router.put('/update/:id', PelangganController.update) // if acessing localhost:3000/update/:id, it will do function update() in pelangganController class
router.delete('/delete/:id', PelangganController.delete) // if acessing localhost:3000/delete/:id, it will do function delete() in pelangganController class

module.exports = router // expost module