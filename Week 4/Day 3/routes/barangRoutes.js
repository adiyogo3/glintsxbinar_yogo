const express = require('express') // import express
const router = express.Router()
const BarangController = require('../controllers/barangController.js')

router.get('/', BarangController.getAll) // if acessing localhost:3000/Barang, it will do function getAll() in BarangController class
router.get('/:id', BarangController.getOne) // if acessing localhost:3000/Barang/:id, it will do function getOne() in BarangController class
router.post('/create', BarangController.create) // if acessing localhost:3000/create, it will do function create() in BarangController class
router.put('/update/:id', BarangController.update) // if acessing localhost:3000/update/:id, it will do function update() in BarangController class
router.delete('/delete/:id', BarangController.delete) // if acessing localhost:3000/delete/:id, it will do function delete() in BarangController class

module.exports = router // expost module