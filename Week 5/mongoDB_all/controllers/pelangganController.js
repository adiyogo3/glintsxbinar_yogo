const { ObjectId } = require('mongodb')
const client = require('../models/connection.js')

class PelangganController {

    async getAll(req, res) {
        const penjualan = client.db('penjualan')
        const pelanggan = penjualan.collection('pelanggan')

        pelanggan.find({}).toArray().then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async getOne(req, res) {
        const penjualan = client.db('penjualan')
        const pelanggan = penjualan.collection('pelanggan')

        pelanggan.findOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async create(req, res) {
        const penjualan = client.db('penjualan')
        const pelanggan = penjualan.collection('pelanggan')

        pelanggan.insertOne({
            nama: req.body.nama,
        }).then(result => {
            res.json({
                status: "new data added to table pelanggan",
                data: result.ops[0]
            })
        })
    }

    async update(req, res) {
        const penjualan = client.db('penjualan')
        const pelanggan = penjualan.collection('pelanggan')

        pelanggan.updateOne({
            _id: new ObjectId(req.params.id)
        }, {
            $set: {
                nama: req.body.nama
            }
        }).then(() => {
            return pelanggan.findOne({
                _id: new ObjectId(req.params.id)
            })
        }).then(result => {
            res.json({
                status: "table pelanggan's data have been sucessfully updated",
                data: result
            })
        })
    }

    async delete(req, res) {
        const penjualan = client.db('penjualan')
        const pelanggan = penjualan.collection('pelanggan')

        pelanggan.deleteOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status: "one of table pelanggan's data have been successfully deleted",
            })
        })
    }
}

module.exports = new PelangganController