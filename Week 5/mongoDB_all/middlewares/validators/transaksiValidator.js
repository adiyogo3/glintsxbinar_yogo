const { check, validationResult, matchedData, sanitize } = require('express-validator')
const { ObjectId } = require('mongodb')
const client = require('../../models/connection.js')

module.exports = {
    create : [
        check('id_barang').custom(value => {
            return client.db('penjualan').collection('barang').findOne({
                _id: new ObjectId(value)
            }).then(result => {
                if (!result) {
                    throw new Error("ID barang doesn't exist")
                }
            })
        }),
        check('id_pelanggan').custom(value => {
            return client.db('penjualan').collection('pelanggan').findOne({
                _id: new ObjectId(value)
            }).then(result => {
                if (!result) {
                    throw new Error("ID pelanggan doesn't exist")
                }
            })
        }),
        check('jumlah').isNumeric().notEmpty(), 
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    update: [
        check('id').custom(value => {
            return client.db('penjualan').collection('transaksi').findOne({
                _id: new ObjectId(value)
            }).then(result => {
                if (!result) {
                    throw new Error("ID transaksi doesn't exist")
                }
            })
        }),
        check('id_barang').custom(value => {
            return client.db('penjualan').collection('barang').findOne({
                _id: new ObjectId(value)
            }).then(result => {
                if (!result) {
                    throw new Error("ID barang doesn't exist")
                }
            })
        }),
        check('id_pelanggan').custom(value => {
            return client.db('penjualan').collection('pelanggan').findOne({
                _id: new ObjectId(value)
            }).then(result => {
                if (!result) {
                    throw new Error("ID pelanggan doesn't exist")
                }
            })
        }),
        check('jumlah').isNumeric(),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next()
        }
    ]
}