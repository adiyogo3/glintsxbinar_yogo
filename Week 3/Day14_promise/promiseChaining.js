// Import fs to read/write file
const fs = require('fs')

/* Start make promise */
const readFile = options => file => new Promise((resolve, reject) => {
  fs.readFile(file, options, (err, content) => {
    if (err) return reject(err)
    return resolve(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})
/* End make promise */


/* Make options variable for fs */
const
  read = readFile('utf-8')

let result = ''
/* End make options variable for fs */


/* Start use promise */
read('konten/konten1.txt')
  .then(konten1 => {
    result += konten1
    return read('konten/konten2.txt') // process to read file content1.txt
  }).then(konten2 => {
    result += konten2
    return read('konten/konten3.txt')
  }).then(konten3 => {
    result += konten3
  }).then(() => {
    console.log('Wikiepedia.id');
    return read('konten/result.txt')
  }).then(result => {
    console.log(result);
  }).catch(err => {
    console.log('Error read/write file, error: ', err);
  })
/* End use promise */
