const login = require(`./login.js`)

let dt =
[
    {
        name : "john" ,
        status : "positive"
    },
    {
        name : "mike" ,
        status : "suspect"
    },
    {
        name : "Tom" ,
        status : "negative"
    },
    {
        name : "jerry" ,
        status : "suspect"
    },
]

function inputoption()
{
    console.log('Menu');
    console.log('====');
    console.log('1. Positive');
    console.log('2. Suspect');
    console.log('3. Negative');
    console.log('4. Exit');
    login.rl.question('input: ', input =>
    {
        switch(Number(input))
        {
            case 1:
                showpositive();
                inputoption();
                break;
            case 2:
                showsuspect();
                inputoption();
                break
            case 3:
                shownegative();
                inputoption();
            case 4:
                login.rl.close()
                break;
            default:
                menu()
        }
    })
}


function showpositive()
{
    console.log("\npositive");
    console.log("=======");
    for (let i = 0 ; i < dt.length ; i ++)
    {
        if (dt[i].status == "positive")
        {
            console.log(`${dt[i].name}`)
        }
    }
    console.log("\n")
}

function showsuspect()
{
    console.log("\nsuspect");
    console.log("=======");
    for (let i = 0 ; i < dt.length ; i ++)
    {
        if (dt[i].status == "suspect")
        {
            console.log(`${dt[i].name}`)
        }
    }
    console.log("\n")
}

function shownegative()
{
    console.log("\nnegative");
    console.log("=======");
    for (let i = 0 ; i < dt.length ; i ++)
    {
        if (dt[i].status == "negative")
        {
            console.log(`${dt[i].name}`)
        }
    }
    console.log("\n")
}



// rl.on("close", () => {
// 	process.exit();
// });

module.exports.inputoption = inputoption