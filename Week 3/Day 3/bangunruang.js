/* Make BangunRuang Abstract Class */
class bangunRuang {

    // Make constructor with name variable/property
    constructor(name) {
      if (this.constructor === bangunRuang) {
        throw new Error('This is abstract!')
      }
  
      this.name = name
    }
  
    // menghitungLuas instance method
    menghitungAlas() {
      console.log('Alas Bangun Ruang');
    }
  
    // menghitungKeliling instance method
    menghitungBangunRuang() {
      console.log('Luas bangun ruang');
    }
  }
  /* End BangunRuang Abstract Class */
  
  module.exports = bangunRuang
  