// Import BangunRuang class
const bangunRuang = require('./bangunruang.js')

/* Make Kubus class that is parent of BangunRuang class (This is inheritance) */
class kubus extends bangunRuang {

  // Make constructor with sisi of persegi
  constructor(sisi) {
    super('Persegi') // call the BanguRuang constructor, so this class will have all variable of the parent classs
    this.sisi = sisi // instance variable
  }

  // Overriding menghitungLuas from BangunDatar class
  menghitungAlas() {
    return this.sisi ** 2
  }

  // Overriding menghitungKeliling from BangunDatar class
  menghitungBangunRuang() {
    return this.sisi ** 3
  }
}
/* End Kubus class */

module.exports = kubus
