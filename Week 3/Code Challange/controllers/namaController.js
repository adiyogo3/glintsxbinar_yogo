// Make HelloController that will be used in helloRoutes
class NamaController {

    // This function will be called in helloRoutes
    async nama(req, res) {
      try {
        console.log('You are accessing nama');
        res.render('nama.ejs') // if success, will be rendering html file from views/hello.ejs
      } catch (e) {
        res.status(500).send(exception) // if error, will be display "500 Internal Server Error"
      }
    }
  
  }
  
  module.exports = new NamaController // We don't need to instance a object bacause exporting this file will be automatically to be an object
  